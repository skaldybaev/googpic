class AuthClient
  def self.client
    OAuth2::Client.new(ENV['OAUTH_CLIENT_ID'], ENV['OAUTH_CLIENT_SECRET'],
                       site: 'https://accounts.google.com/o/oauth2/auth',
                       authorize_url: '/o/oauth2/auth',
                       token_url: '/o/oauth2/token',
                       redirect_path: 'http://googpic.herokuapp.com/auth/googpic/callback',
                       response_type: 'code',
                       scope: 'https://picasaweb.google.com/data',
                       access_type: 'offline'
    )
  end

  def self.authorize_url
    client.authorize_url({client_id: ENV['OAUTH_CLIENT_ID'],
                          redirect_uri: 'http://googpic.herokuapp.com/auth/googpic/callback',
                          scope: 'https://picasaweb.google.com/data',
                          response_type: 'code'
                         })
  end

  def self.get_token(code)
    client.get_token({code: code,
                      client_id: ENV['OAUTH_CLIENT_ID'],
                      client_secret: ENV['OAUTH_CLIENT_SECRET'],
                      redirect_uri: 'http://googpic.herokuapp.com/auth/googpic/callback',
                      grant_type: 'authorization_code'
                     })
  end
end