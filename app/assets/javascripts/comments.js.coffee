jQuery ->
  $('#myModal').on 'shown.bs.modal', (e) ->
    modal = $(e.target).find('.modal-body')
    idArray = $(e.relatedTarget).data('ids').split('_');
    albumId = idArray[1]
    photoId = idArray[2]

    $.getJSON albumId + "/photos/" + photoId + "/comments", (data) ->
      items = []
      $.each data, (key, val) ->
        items.push "<li id='" + key + "'>" + val + "</li>"

      $("<ul/>",
        class: "comments-list"
        html: items.join("")
      ).appendTo modal

  $('#myModal').on 'hide.bs.modal', (e) ->
    modal = $(e.target).find('.modal-body')
    modal.html('')