class Photo
  attr_accessor :id, :album_id, :thumb, :name, :count

  def initialize(hash={})
    @id = hash.fetch(:id)
    @album_id = hash.fetch(:album_id)
    @thumb = hash.fetch(:thumb)
    @name = hash.fetch(:name)
    @count = hash.fetch(:count)
  end

  def self.list(token, album_id, limit=3)
    response = access_token(token).get "https://picasaweb.google.com/data/feed/api/user/default/albumid/#{album_id}", {parse: :json, mode: :query}
    doc = Nokogiri::XML(response.body)
    photos = []

    doc.css('entry').each do |entry|
      photos << new({id: entry.at_xpath('gphoto:id').content,
                     album_id: entry.at_xpath('gphoto:albumid').content,
                     name: entry.at_css('title').content,
                     count: entry.at_xpath('gphoto:commentCount').content,
                     thumb: entry.xpath('media:group/media:thumbnail/@url').last.content
                    })
    end
    photos.take(limit)
  end

  def self.comments(token, album_id, photo_id)
    response = access_token(token).get "https://picasaweb.google.com/data/feed/api/user/default/albumid/#{album_id}/photoid/#{photo_id}?kind=comment", {parse: :json, mode: :query}
    doc = Nokogiri::XML(response.body)
    comments = []

    doc.css('entry').each do |entry|
      comments << entry.css('content').inner_text
    end

    comments
  end

  def self.add_comment(token, album_id, photo_id, comment)
    access_token = access_token(token)

    post_data = """<entry xmlns='http://www.w3.org/2005/Atom'>
      <content>#{comment}</content>
      <category scheme='http://schemas.google.com/g/2005#kind'
        term='http://schemas.google.com/photos/2007#comment'/>
    </entry>"""
    access_token.post("https://picasaweb.google.com/data/feed/api/user/default/albumid/#{album_id}/photoid/#{photo_id}",
                      body: post_data, headers: {'Content-Type' => 'application/atom+xml', 'Gdata-version' => '2'})
  end

  private

  def self.access_token(token)
    OAuth2::AccessToken.new(AuthClient.client, token)
  end

  def access_token(token)
    OAuth2::AccessToken.new(AuthClient.client, token)
  end
end