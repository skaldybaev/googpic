class Album
  def self.list(token)
    access_token = OAuth2::AccessToken.new(AuthClient.client, token)
    response = access_token.get 'https://picasaweb.google.com/data/feed/api/user/default', {parse: :json, mode: :query}

    doc = Nokogiri::XML(response.body)
    albums = {}

    doc.css('entry').each do |entry|
      albums.store(entry.at_xpath('gphoto:id').content, entry.at_css('title').content)
    end

    albums
  end
end