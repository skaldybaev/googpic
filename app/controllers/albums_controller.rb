class AlbumsController < ApplicationController
  before_filter :authenticate

  def index
    @albums = Album.list(session[:token])
  end

  def show
    @photos = Photo.list(session[:token], params[:id])
  end
end