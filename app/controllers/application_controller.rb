class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private
  def authenticate
    unless session[:token] && DateTime.strptime(session[:token_valid_until].to_s, '%s') > DateTime.now
      redirect_to root_path
    end
  end
end
