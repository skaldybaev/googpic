class SessionsController < ApplicationController
  def create
    begin
      token = AuthClient.get_token(params[:code])
      session[:token] = token.token
      session[:token_valid_until] = token.expires_at
    rescue OAuth2::Error => e
      raise "#{e}"
    end

    redirect_to albums_path
  end
end
