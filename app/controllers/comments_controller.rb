class CommentsController < ApplicationController
  before_filter :authenticate

  def index
    @comments = Photo.comments(session[:token], params[:album_id], params[:photo_id])
    render json: @comments.to_json(root: false)
  end

  def create
    @comment = Photo.add_comment(session[:token], params[:album_id], params[:photo_id], params[:comment])
  end
end
